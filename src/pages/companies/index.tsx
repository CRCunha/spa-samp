import React, { useEffect, useState } from 'react';
import Header from '../../components/header';
import useStyles from './styles';
import Main from '../../components/main';
import ButtonLeftSideMenu from '../../components/buttonMenu';
import api from '../../services/api';
import { Icon } from 'semantic-ui-react';
import Slider from 'react-slick';
import { motion } from 'framer-motion';

//https://cdn.dribbble.com/users/1919332/screenshots/8768113/media/cf6fd068965e228567c3c7c7fab7f085.jpg

const Companies = () => {
  const classes = useStyles();
  const [companies, setCompanies] = useState();

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    speed: 400,
    autoplaySpeed: 7000,
    cssEase: 'linear',
  };

  useEffect(() => {
    api
      .get('/getCompanies')
      .then((response) => setCompanies(response.data))
      .catch((err) => {
        console.error('ops! ocorreu um erro' + err);
      });
  }, []);

  return (
    <div className={classes.main}>
      <Header />
      <div className={classes.content}>
        <div className={classes.leftSide}>
          <div className={classes.leftSideMenuContainer}>
            <div className={classes.menu}>
              <div className={classes.menuTextContainer}>
                <span className="font-bold text-lg text-gray-400 font-sans">
                  Devirse Roleplay
                </span>
                <div className={classes.buttonsContainerLine}>
                  <div className={classes.buttonsLine} />
                </div>
              </div>
              <div className={classes.buttonsContainer}>
                <ButtonLeftSideMenu icon="home" link="/" delay={0.25} />
                <ButtonLeftSideMenu
                  icon="building"
                  link="/companies"
                  delay={0.35}
                />
                <ButtonLeftSideMenu icon="map" link="#" delay={0.45} />
                <ButtonLeftSideMenu icon="cog" link="#" delay={0.55} />
              </div>
            </div>
          </div>
          <Main />
        </div>
        <div className={classes.rightSide}>
          <Slider className={classes.slide} {...settings}>
            {companies?.map(
              (company: {
                companyId: number;
                companyType: 'string';
                companyName: 'string';
                id: React.Key | null | undefined;
              }) => (
                <motion.div
                  initial={{ opacity: 0, scale: 0.8 }}
                  animate={{ opacity: 1, scale: 1 }}
                  transition={{
                    delay: company.companyId / 2.8,
                    scale: {
                      type: 'spring',
                      damping: 10,
                      stiffness: 80,
                      restDelta: 0.001,
                    },
                  }}
                >
                  <div className={classes.cardCompany} key={company.id}>
                    <div className={classes.cardCompanyHeader}>
                      {company.companyName}
                    </div>
                    <div className={classes.logoCompanyContainer}>
                      <Icon name="building" size="big" />
                    </div>
                    <div className={classes.cardCompanyFooter}>
                      {company.companyType}
                    </div>
                  </div>
                </motion.div>
              ),
            )}
          </Slider>
        </div>
      </div>
    </div>
  );
};

export default Companies;
