import { makeStyles } from '@material-ui/styles';
import backgroundImage from '../../assets/images/background.png';
import asideImage from '../../assets/images/asideImage.png';

const useStyles = makeStyles({
  main: {
    height: '100vh',
    width: '100vw',
    backgroundImage: `url(${backgroundImage})`,
    overflow: 'hidden',
  },
  content: {
    marginTop: '-100px',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  leftSide: {
    width: '40%',

    display: 'flex',
  },
  leftSideMenuContainer: {
    width: '110px',
    height: '100%',

    display: 'flex',
    alignItems: 'center',
  },
  menu: {
    width: '100%',
    height: '72%',
  },
  menuTextContainer: {
    width: '110px',
    height: '50%',

    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'column',

    '& span': {
      width: '140%',
      height: '108px',
      transform: 'rotate(90deg)',
    },
  },
  buttonsContainerLine: {
    width: '100%',
    height: '180px',

    marginTop: '30px',

    display: 'flex',
    justifyContent: 'center',
  },
  buttonsLine: {
    width: '4px',
    height: '100%',

    marginLeft: '28px',

    backgroundColor: '#9ca3af30',
  },
  buttonsContainer: {
    width: '100%',
    height: '50%',

    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  rightSide: {
    width: '60%',
    backgroundSize: '75%',
    backgroundRepeat: 'no-repeat',
    backgroundPositionY: 'center',
    backgroundPositionX: '75%',
    backgroundImage: `url(${asideImage})`,
  },
});

export default useStyles;
