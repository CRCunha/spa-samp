import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  button: {
    height: '80px',
    width: '80px',
    backgroundColor: '#fff',
    boxShadow: '0 20px 30px -16px rgba(9,9,16,0.15)!important;',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    color: 'rgba(9,9,16,0.15)',
    fontSize: '22pt',
  },
});

export default useStyles;
