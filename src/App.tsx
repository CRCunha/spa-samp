import Router from './routes';
import './app.css';
import error from './assets/images/404.jpeg';
//http://cristianapp.com.br.s3-website-sa-east-1.amazonaws.com/

const App = () => {
  return (
    <>
      <div className="App">
        <Router>
          <Router />
        </Router>
      </div>
      <div className="AppNone">
        <img src={error} id="errorImage" alt="404" />
      </div>
    </>
  );
};

export default App;
