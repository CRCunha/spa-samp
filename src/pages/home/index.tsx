import Header from '../../components/header';
import useStyles from './styles';
import Main from '../../components/main';
import ButtonLeftSideMenu from '../../components/buttonMenu';
//https://cdn.dribbble.com/users/1919332/screenshots/8768113/media/cf6fd068965e228567c3c7c7fab7f085.jpg

const Home = () => {
  const classes = useStyles();
  return (
    <div className={classes.main}>
      <Header />
      <div className={classes.content}>
        <div className={classes.leftSide}>
          <div className={classes.leftSideMenuContainer}>
            <div className={classes.menu}>
              <div className={classes.menuTextContainer}>
                <span className="font-bold text-lg text-gray-400 font-sans">
                  Devirse Roleplay
                </span>
                <div className={classes.buttonsContainerLine}>
                  <div className={classes.buttonsLine} />
                </div>
              </div>
              <div className={classes.buttonsContainer}>
                <ButtonLeftSideMenu icon="home" link="/" delay={0.25} />
                <ButtonLeftSideMenu
                  icon="building"
                  link="/companies"
                  delay={0.35}
                />
                <ButtonLeftSideMenu icon="map" link="#" delay={0.45} />
                <ButtonLeftSideMenu icon="cog" link="#" delay={0.55} />
              </div>
            </div>
          </div>
          <Main />
        </div>
        <div className={classes.rightSide} />
      </div>
    </div>
  );
};

export default Home;
