import useStyles from './styles';
import { motion } from 'framer-motion';
import 'semantic-ui-css/semantic.min.css';
import { Icon } from 'semantic-ui-react';
import { useNavigate } from 'react-router-dom';
//https://react.semantic-ui.com/elements/icon/

interface ButtonLeftSideMenuProps {
  link: string;
  delay: number;
  icon: string;
}

const Home = (props: ButtonLeftSideMenuProps) => {
  const classes = useStyles();
  const navigate = useNavigate();

  return (
    <motion.div
      whileHover={{
        scale: 1.05,
        transition: { duration: 0.1 },
      }}
      initial={{ opacity: 0, scale: 0.5 }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{
        default: {
          duration: 0.3,
          ease: [0, 0.71, 0.2, 1.01],
        },
        delay: props.delay,
        scale: {
          type: 'spring',
          damping: 10,
          stiffness: 80,
          restDelta: 0.001,
        },
      }}
    >
      <a
        onClick={() => navigate(props.link)}
        target="_blank"
        className={classes.button}
      >
        <Icon name={props.icon} size="big" />
      </a>
    </motion.div>
  );
};

export default Home;
