import React from 'react';
import { Routes, Route } from 'react-router-dom';
import '../App';

// Pages
import Home from '@/pages/home';
import Companies from '@/pages/companies';
import Login from '@/pages/login';

const Router = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/companies" element={<Companies />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </div>
  );
};

export default Router;
