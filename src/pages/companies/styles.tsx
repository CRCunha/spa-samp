import { makeStyles } from '@material-ui/styles';
import backgroundImage from '../../assets/images/background.png';
import companyCard from '../../assets/images/companyCard.png';

const useStyles = makeStyles({
  main: {
    height: '100vh',
    width: '100vw',
    backgroundImage: `url(${backgroundImage})`,
    overflow: 'hidden',
  },
  content: {
    marginTop: '-100px',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  leftSide: {
    width: '40%',

    display: 'flex',
  },
  leftSideMenuContainer: {
    width: '110px',
    height: '100%',

    display: 'flex',
    alignItems: 'center',
  },
  menu: {
    width: '100%',
    height: '72%',
  },
  menuTextContainer: {
    width: '110px',
    height: '50%',

    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'column',

    '& span': {
      width: '140%',
      height: '108px',
      transform: 'rotate(90deg)',
    },
  },
  buttonsContainerLine: {
    width: '100%',
    height: '180px',

    marginTop: '30px',

    display: 'flex',
    justifyContent: 'center',
  },
  buttonsLine: {
    width: '4px',
    height: '100%',

    marginLeft: '28px',

    backgroundColor: '#9ca3af30',
  },
  buttonsContainer: {
    width: '100%',
    height: '50%',

    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  rightSide: {
    width: '60%',
    maxWidth: '60%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardCompany: {
    minWidth: '442px',
    minHeight: '462px',
    backgroundImage: `url(${companyCard})`,
    backgroundSize: 'cover',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  slide: {
    width: '980px !important',
    height: '460px !important',
  },

  cardCompanyHeader: {
    width: '70%',
    height: '50px',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: '#fff',

    transform: 'skew(-20deg)',

    marginBottom: '25px',

    fontSize: '18px',
    color: '#aab2c1',
    fontWeight: 'bold',
  },

  logoCompanyContainer: {
    width: '80px',
    height: '80px',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    borderRadius: '100%',

    backgroundColor: '#fff',

    marginBottom: '20px',
  },

  cardCompanyFooter: {
    width: '60%',
    height: '30px',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    fontSize: '18px',
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default useStyles;
