/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        brand: {
          500: '#8257e6',
          600: '#996dff',
        },
      },
    },
  },
  plugins: [],
};
